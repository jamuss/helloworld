/* eslint-disable no-undef */
/* eslint-disable import/extensions */
const request = require('supertest');
const app = require('../src/server.js');

describe('GET /', () => {
  it('displays I\'m connecting the repo! Again!', (done) => {
    // The line below is the core test of our app.
    request(app).get('/').expect('I\'m connecting the repo! Again!', done);
  });
});
