const express = require('express');

const port = process.env.PORT || 80;

const app = express();
// The code below will display 'Hello World!' to the browser when you go to http://localhost:3000
app.get('/', (req, res) => {
  res.send('I\'m connecting the repo! Again!');
});

app.get('/bitbucket', (req, res) => {
  res.send('Beep Boop Bitbucket!');
});
app.listen(port, () => { console.log(`Example app listening on port ${port}!`); });
module.exports = app;
